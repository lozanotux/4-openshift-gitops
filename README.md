# OpenShift GitOps

Este repositorio contiene todo lo necesario para configurar **OpenShift GitOps** en cl cluster correspondiente. De esta manera se automatiza la creación de todas las apps y sus respectivas dependencias.

## **Paso 1 (Instalación del Operador)**

1. Desde la consola web de OpenShift, nos dirigimos a la seccion **Operators --> OperatorHub**. Buscamos el operador llamado `Red Hat OpenShift GitOps` y lo instalamos. El operador crea un namespace `openshift-gitops` y dentro una serie de CRD (hay que esperar).

2. Hay que crear un par de `rbac` con los permisos necesarios para que `Openshift GitOps` pueda manipular los namespaces dentro del cluster de OpenShift. Para ello ejecutamos el siguiente comando:
    ```bash
    $ oc create -f ./cluster-1/core/rolebindings.yaml
    ```

## **Paso 2 (Configuración)**

1. Creamos un template de credenciales para todos los repositorios que administrará ArgoCD. Para ello desde la consola web de ArgoCD, desde la sección **Configuraciones ⚙ --> Repositorios** se debe crear un nuevo repositorio. Y en vez de hacer clic en el botón `[Conectar]`, se deben guardar las credenciales haciendo clic en el botón `[Save as Credentials Template]`.
![credentials-template](images/openshift-gitops-credentials-template.png)

> **NOTA 1:** tener en cuenta que cada template de credenciales configurado usa un prefijo en la URL. Por lo cual, esas credenciales solo serán validas para todos aquellos repositorios Git que coincidan con aquel prefijo.

> **NOTA 2:** para conocer más detalles de esta característica, visite el siguiente enlace: [argo-cd.readthedocs.io/repositories#credential-templates](https://argo-cd.readthedocs.io/en/stable/user-guide/private-repositories/#credential-templates)

2. Configuramos `Openshift GitOps` registrando este repositorio Git `4-openshift-gitops` a través del Secret [repository-openshift-gitops.yaml](./cluster-1/core/repository-openshift-gitops.yaml). Para esto, ejecutamos el siguiente comando:
    ```bash
    $ oc create -f ./cluster-1/core/repository-openshift-gitops.yaml
    ```

3. Registramos un proyecto `ArgoCD/AppProject` necesario donde se guardará una `ArgoCD/Application` que se encargá de registrar automaticamente las aplicaciones. Para ello, ejecutamos los siguientes comandos:
    ```bash
    $ oc create -f ./cluster-1/core/project-openshift-gitops.yaml
    $ oc create -f ./cluster-1/core/applications.yaml
    ```

    > **NOTA:** _La **ArgoCD/Application** `applications.yaml` es la encargada de monitorear las demás aplicaciónes._

## **Paso 3 (Registrar nuevas aplicaciones)**

1. Para registrar nuevas aplicaciones, creamos la estructura necesaria para nuestro proyecto `project1` con todos sus directorios.

La información esta separada por `cluster` / `proyecto` / `aplicación`. La estructura deberia quedar de la siguiente manera:
```
.
├── cluster-1
|   ├── core
│   │   └── rolebinding.yaml
│   └── proyectos
│       ├── proyecto1
│       │   ├── dev
│       │   │   ├── apps
│       │   │   │   └── example-app1.yaml
│       │   │   └── project
│       │   │       ├── limit-range.yaml
│       │   │       ├── namespace.yaml
│       │   │       ├── resource-quota.yaml
│       │   │       └── role-binding.yaml
│       │   └── test
│       │       ├── apps
│       │       │   └── example-app1.yaml
│       │       └── project
│       │           └── namespace.yaml
│       └── project1-project.yaml
└── cluster-2
```

Por ejemplo el proyecto `proyecto1` del cluster `cluster-1` esta compuesto de 2 ambientes (`dev` y `test`) y contiene la aplicación `example-app1`.

En el ambiente `dev` del projecto `proyecto1` se declara la creación del `namespace` y configuración de `limits`, `quota`, `rolebinding`. En cambio en el ambiente de `test` solo se creó el `namespace`. De esta manera podremos separar que cosas adicionales necesita cada **namespace** dentro de OpenShift.

2. Verificación

Si todo esta bien configurado, deberias ver en la consola de `Openshift GitOps` lo siguiente:
![](images/openshift-gitops-success1.png)

Y en OpenShift deberías ver que ya se crearon los `namespaces` con los objetos declarados en cada uno.
![](images/openshift-gitops-success2.png)

> **NOTA:** _Prueba en borrar algunos de los objetos registrados en este repositorio! `Openshift GitOps` los va a regenerar nuevamente de forma automática. Observa la consola de `Openshift GitOps` para ver el progreso._